import { Component , OnInit} from '@angular/core';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { HttpClient } from '@angular/common/http';

interface Parking{
   id ? : number;
   lot: number;
   vehicleNumber : number;
   parkingDuration: number;
   parkingAmount ?: number;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent{

  apiUrl = "http://localhost:8080/"
  vehicleForm : FormGroup;
  parkings : Parking[];
  minAmount = 20;
  subsequentHrPerMin = 0.333;
  errorAlert = false;
  error_message;

  constructor(private http:HttpClient) {
      this.vehicleForm = this.createForm();
      this.getAllParking();
  }

  /**
   * Create a form 
   * 
   */
  createForm(){
    return new FormGroup({
      vehicleLot: new FormControl('', [Validators.required]),
      vehicleNumber: new FormControl('', [Validators.required]),
      vehicleDuration: new FormControl('', Validators.required),
      vehicleAmount: new FormControl()
    });
  }

  /**
   * On Sumit park new vehicle
   * 
   */
  onSubmit():void{
    console.log(this.vehicleForm.getRawValue());
    let vehicle : Parking = {
        lot : this.vehicleForm.get('vehicleLot').value,
        parkingDuration : this.vehicleForm.get('vehicleDuration').value,
        vehicleNumber : this.vehicleForm.get('vehicleNumber').value
    }
    this.parkNewvehicle(vehicle);
  }

  /**
   * Parking new vehicle
   * 
   * @param vehicle 
   */
  parkNewvehicle(vehicle){
    this.reset();
    let api = this.getApiUrl('api/parkings');
    this.http.post<Parking>(api, vehicle).subscribe(
      (response) => {
          this.getAllParking();
      },
      (error) => {
          this.handleError(error)
      }
    );
  }

  /**
   * Get All Parking as list
   * 
   */
  getAllParking(){
    let api = this.getApiUrl('api/parkings');
    this.http.get<Parking[]>(api).subscribe(data => {
      this.parkings = data
    })
  }

  /**
   * Handle Error from API
   * 
   * @param response 
   */
  handleError(response: Response | any){
    this.errorAlert = true;
    this.error_message = response.error.message;
  }

  /**
   * Reset couple of values
   */
  reset(){
    this.errorAlert = false;
    this.error_message = null;
  }

  /**
   * Calculate Amount on keyup
   * @param e 
   */
  calculateAmount(e){
    let cost = 0;
    let duration = e.target.value;
    if(duration <= 60){
      cost =  this.minAmount;
    }else{
       cost = Math.round(((duration - 60) * this.subsequentHrPerMin ) + this.minAmount);
    }

    this.vehicleForm.get('vehicleAmount').setValue(cost);
  }

  /**
   * Get API Url
   * @param api
   */
  getApiUrl(api){
    return this.apiUrl + api; 
  }
}
