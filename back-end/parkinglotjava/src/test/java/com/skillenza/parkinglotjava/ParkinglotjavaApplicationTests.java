package com.skillenza.parkinglotjava;

import java.net.URI;
import java.net.URISyntaxException;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultHandler;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ParkinglotjavaApplicationTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ParkingLotRepository parkingLotRepository;

    private static final ObjectMapper om = new ObjectMapper();

    @Test
    public void testFindAll() throws Exception {
        mockMvc.perform(get("/api/parkings"))
                .andExpect(status().isOk());
    }

    @Test
    public void testSave() throws Exception{
        mockMvc.perform(post("/api/parkings")
                .contentType("application/json")
                .content(getPayload()))
        .andExpect(status().isCreated());
    }

    @Test
    public void testSaveAndFindAll() throws Exception{

        mockMvc.perform(post("/api/parkings")
                .contentType("application/json")
                .content(getPayload()))
        .andExpect(status().isCreated());

        mockMvc.perform(get("/api/parkings"))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[*].id").isNotEmpty());
    }

    @Test
    public void testSaveWithBadRequest() throws Exception{


        mockMvc.perform(post("/api/parkings")
                .contentType("application/json")
                .content(getPayload()))
        .andExpect(status().isCreated());

        mockMvc.perform(post("/api/parkings")
                .contentType("application/json")
                .content(getPayload()))
        .andExpect(status().isBadRequest());

    }

    @After
    public void tearDown(){
        parkingLotRepository.deleteAll();
    }

    private String getPayload(){
        String payload = "{\n" +
                "\t\"lot\" : 1,\n" +
                "\t\"vehicleNumber\" : 1003,\n" +
                "\t\"parkingDuration\": 65\n" +
                "}";

        return payload;
    }

}
