package com.skillenza.parkinglotjava;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class ParkingException extends RuntimeException {

    public ParkingException(String message) {
        super(message);
    }
}
