package com.skillenza.parkinglotjava;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.print.attribute.standard.Media;
import javax.validation.Valid;
import java.util.List;
import java.util.Set;


@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api")
public class ParkingLotController {

    private ParkingLotRepository parkingLotRepository;

    // this value is configurable and we can fetch from properties
    @Value("${parking.minute.charges}")
    public int MIN_CHARGES_FOR_60_MIN ;

    @Value("${parking.subsequent.minute.charges}")
    public double SUBSEQUENT_CHARGES_HOUR_PER_MIN;


    @Autowired
    public ParkingLotController(ParkingLotRepository parkingLotRepository) {
        this.parkingLotRepository = parkingLotRepository;
    }

    /**
     * GET /parkings API
     * @return
     */
    @GetMapping(path = "/parkings"
            , produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<ParkingLot>> findAll() {
        List<ParkingLot> parkingLots = parkingLotRepository.findAll();
        return new ResponseEntity(parkingLots, HttpStatus.OK);
    }

    /**
     * POST /parkings API
     * @param parkingLot
     * @return
     */
    @PostMapping(path = "/parkings"
            , consumes = MediaType.APPLICATION_JSON_VALUE
            , produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ParkingLot> save(@Valid @RequestBody ParkingLot parkingLot) {
        int parkingAmount = calculateParkingAmount(parkingLot.getParkingDuration());
        parkingLot.setParkingAmount(parkingAmount);

        // persist parking lot
        ParkingLot savedParkingLot = null;
        try{
            savedParkingLot = parkingLotRepository.save(parkingLot);
        }catch (DataIntegrityViolationException e){
            //we can have better exception handling mechanism. message can be configurable
            // this I have added as new file
            throw new ParkingException("VEHICLE ALREADY PARKED");
        }
        return new ResponseEntity<ParkingLot>(savedParkingLot, HttpStatus.CREATED);
    }

    /**
     * Calculate parking amount based on parking duration
     *
     * @param parkingDurationInMins
     * @return
     */
    private int calculateParkingAmount(int parkingDurationInMins){
        if(parkingDurationInMins <=60){
            return MIN_CHARGES_FOR_60_MIN;
        }

        int remainingMins = parkingDurationInMins - 60;
        long cost = Math.round(remainingMins * SUBSEQUENT_CHARGES_HOUR_PER_MIN ) + MIN_CHARGES_FOR_60_MIN;
        return new Long(cost).intValue();
    }
}

